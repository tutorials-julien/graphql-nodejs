# Exemple API GraphQL en NodeJS

## Prérequis

Avoir docker et docker-compose, ou alors un serveur de base de donnée SQL.
Avoir NodeJS et NPM.

## 1. Créer une base de donnée

Lancer la commande docker-compose-up.

Se connecter à la base donnée et éxécuter le script ./models/create_tables.sql sur la base de donnée créée.

Vous pouvez aussi mettre en place une base donnée en local.

## 3. Installer les dépendances

Exécuter la commande suivante

```sh
$ npm install
```

## 3. Lancer l'API

Exécuter la commande suivante

```sh
$ npm start
```