const { ApolloServer } = require('apollo-server');

const config = require('./config');

require('./server/database');

const schema = require('./server/graphql');

const server = new ApolloServer({
  schema,
  introspection: true,
  playground: true,
  context: () => {
    return {
      context: 'context'
    }
  }
});

server.listen({
  port: config.application.port
}).then(({ url, subscriptionsUrl }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});