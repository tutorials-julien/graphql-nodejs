'use strict';

module.exports = {
    environment: process.env.ENVIRONMENT || 'dev',
    database: {
        database: process.env.DATABASE || 'graphql-database',
        host: process.env.DATABASE_HOST || 'localhost',
        port: process.env.DATABASE_PORT || '3306',
        user: process.env.DATABASE_USER || 'graphql',
        password: process.env.DATABASE_PASSWORD || 'password'
    },
    application: {
        port: parseInt(process.env.PORT, 10) || 3000,
        graphqlEndpoint: process.env.APPLICATION_GRAPHQL_ENDPOINT || '/graphql'
    }
}