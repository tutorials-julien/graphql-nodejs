module.exports = function(sequelize, DataTypes) {
	return sequelize.define(
		'beer',
		{
			id: {
				type: DataTypes.INTEGER(11),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			name: {
                type: DataTypes.STRING(128),
				allowNull: false
            },
			description: {
				type: DataTypes.STRING(512),
				allowNull: false
			},
			alcohol: {
				type: DataTypes.DOUBLE,
				allowNull: false
            },
            price: {
                type: DataTypes.DOUBLE,
                allowNull: false
            }
		},
		{
			tableName: 'beer',
			timestamps: false
		}
	);
};
