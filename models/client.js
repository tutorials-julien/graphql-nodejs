module.exports = function(sequelize, DataTypes) {
	return sequelize.define(
		'client',
		{
			id: {
				type: DataTypes.INTEGER(11),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			firstname: {
                type: DataTypes.STRING(128),
				allowNull: false
            },
			lastname: {
				type: DataTypes.STRING(128),
				allowNull: false
			},
			email: {
				type: DataTypes.STRING(128),
				allowNull: false
			}
		},
		{
			tableName: 'client',
			timestamps: true
		}
	);
};
