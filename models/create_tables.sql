SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE TABLE `beer` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `alcohol` double NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB;


CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

CREATE TABLE `drink` (
  `id` int(11) NOT NULL,
  `beerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  `isPayed` tinyint(1) DEFAULT '0',
  `quantity` int(2) DEFAULT '1',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

CREATE TABLE `opinion` (
  `id` int(11) NOT NULL,
  `beerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  `opinion` text,
  `note` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

ALTER TABLE `beer`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `drink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `beerId` (`beerId`),
  ADD KEY `clientId` (`clientId`);

ALTER TABLE `opinion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `beerId` (`beerId`),
  ADD KEY `clientId` (`clientId`);

ALTER TABLE `beer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `drink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `opinion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `drink`
  ADD CONSTRAINT `drink_ibfk_1` FOREIGN KEY (`beerId`) REFERENCES `beer` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `drink_ibfk_2` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`) ON UPDATE CASCADE;

ALTER TABLE `opinion`
  ADD CONSTRAINT `opinion_ibfk_1` FOREIGN KEY (`beerId`) REFERENCES `beer` (`id`),
  ADD CONSTRAINT `opinion_ibfk_2` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`);
  
COMMIT;