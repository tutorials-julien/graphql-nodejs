module.exports = function(sequelize, DataTypes) {
	return sequelize.define(
		'drink',
		{
			id: {
				type: DataTypes.INTEGER(11),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			beerId: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                references: {
					model: 'beer',
					key: 'id'
				}
            },
            clientId: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                references: {
					model: 'client',
					key: 'id'
				}
            },
			isPayed: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            quantity: {
                type: DataTypes.INTEGER(2),
                defaultValue: 1
            }
		},
		{
			tableName: 'drink',
			timestamps: true
		}
	);
};
