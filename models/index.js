const path = require('path');

module.exports = function(sequelize) {
	const Beer = sequelize.import(path.join(__dirname, 'beer'));
	const Client = sequelize.import(path.join(__dirname, 'client'));
	const Drink = sequelize.import(path.join(__dirname, 'drink'));
	const Opinion = sequelize.import(path.join(__dirname, 'opinion'));

    Drink.belongsTo(Client, {
        foreignKey: 'clientId',
        targetKey: 'id'
    });

    Drink.belongsTo(Beer, {
        foreignKey: 'beerId',
        targetKey: 'id'
    });

    Beer.hasMany(Drink, {
        foreignKey: 'beerId',
        sourceKey: 'id'
    });

    Client.hasMany(Drink, {
        foreignKey: 'clientId',
        sourceKey: 'id'
    });

	return {
        Client,
        Beer,
        Drink,
        Opinion
	};
};
