module.exports = function(sequelize, DataTypes) {
	return sequelize.define(
		'opinion',
		{
			id: {
				type: DataTypes.INTEGER(11),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			beerId: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                references: {
					model: 'beer',
					key: 'id'
				}
            },
            clientId: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                references: {
					model: 'client',
					key: 'id'
				}
            },
            opinion: {
                type: DataTypes.TEXT
            },
			note: {
                type: DataTypes.INTEGER,
                defaultValue: false,
                validate: { min: 0, max: 10 }
            }
		},
		{
			tableName: 'opinion',
			timestamps: true
		}
	);
};
