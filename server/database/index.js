const Sequelize = require('sequelize');
const initModels = require('../../models');
const config = require('../../config');

function initDatabase() {
	const { host, port, user, password, database } = config.database;
	const sequelize = new Sequelize(database, user, password, {
		host,
		dialect: 'mysql',
		pool: {
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000
		},
		port,
		define: {
			timestamps: false
		},
		timezone: 'Europe/Paris'
	});

	sequelize
		.authenticate()
		.then(() => {
			console.log('OK');
		})
		.catch(e => {
			console.error(e);
		});

	return sequelize;
}

const models = setUpModels();

module.exports = models;

function setUpModels() {
	const database = initDatabase();

	const { Beer, Drink, Client, Opinion } = initModels(database);

	return { Beer, Drink, Client, Opinion, database };
}
