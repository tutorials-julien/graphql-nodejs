
const { readFileSync } = require('fs');
const { makeExecutableSchema } = require('graphql-tools');

const path = require('path');

const { pubsub, DRINK_ADDED, CLIENT_ADDED } = require('./subscription.service');
const { opinionService, drinkService, beerService, clientService } = require('../services');

module.exports = makeExecutableSchema({
    typeDefs: readFileSync(path.join(__dirname, 'schema.graphql'), 'utf8'),
    resolvers: {
        Query: {
            beers: (_, filters, ctx) => beerService.getBeers(filters),
            beer: (_, filters, ctx) => beerService.getBeer(filters.id),

            clients: (_, filters, ctx) => clientService.getClients(filters),
            client: (_, filters, ctx) => clientService.getClient(filters.id),

            opinions: (_, filters, ctx) => opinionService.getOpinions(filters),
            opinion: (_, filters, ctx) => opinionService.getOpinion(filters.id),
            averageOpinion: (_, filters, ctx) => opinionService.getAverageOpinion(filters.beerId),

            drinks: (_, filters, ctx) => drinkService.getDrinks(filters),
            drink: (_, filters, ctx) => drinkService.getDrink(filters.id)
        },

        Mutation: {
            createBeer: (_, filters, ctx) => beerService.createBeer(filters.input),
            updateBeer: (_, filters, ctx) => beerService.updateBeer(filters.id, filters.input),
            deleteBeer: (_, filters, ctx) => beerService.deleteBeer(filters.id),

            createClient: (_, filters, ctx) => clientService.createClient(filters.input),
            updateClient: (_, filters, ctx) => clientService.updateClient(filters.id, filters.input),
            deleteClient: (_, filters, ctx) => clientService.deleteClient(filters.id),
            payAll: (_, filters, ctx) => drinkService.payAll(filters.clientId),

            createDrink: (_, filters, ctx) => drinkService.createDrink(filters.input),
            updateDrink: (_, filters, ctx) => drinkService.updateDrink(filters.id, filters.input),
            deleteDrink: (_, filters, ctx) => drinkService.deleteDrink(filters.id),

            createOpinion: (_, filters, ctx) => opinionService.createOpinion(filters.input),
            updateOpinion: (_, filters, ctx) => opinionService.updateOpinion(filters.id, filters.input),
            deleteOpinion: (_, filters, ctx) => opinionService.deleteOpinion(filters.id)
        },

        Subscription: {
            drinkAdded: {
                subscribe: () => pubsub.asyncIterator([DRINK_ADDED]),
            },
            clientAdded: {
                subscribe: () => pubsub.asyncIterator([CLIENT_ADDED]),
            }
        },

        Opinion: {
            beer: (opinion, filters, ctx) => beerService.getBeer(opinion.beerId),
            client: (opinion, filters, ctx) => clientService.getClient(opinion.clientId)
        },

        Drink: {
            beer: (drink, filters, ctx) => beerService.getBeer(drink.beerId),
            client: (drink, filters, ctx) => clientService.getClient(drink.clientId)
        },

        Client: {
            opinions: (client, filters, ctx) => opinionService.getOpinions(Object.assign({}, filters, { clientId: client.id })),
            drinks: (client, filters, ctx) => drinkService.getDrinks(Object.assign({}, filters, { clientId: client.id })),
            facture: (client, filters, ctx) => drinkService.getFacture(client.id)
        },

        Beer: {
            opinions: (beer, filters, ctx) => opinionService.getOpinions(Object.assign({}, filters, { beerId: beer.id })),
            drinks: (beer, filters, ctx) => drinkService.getDrinks(Object.assign({}, filters, { beerId: beer.id })),
            averageOpinion: (beer, filters, ctx) => opinionService.getAverageOpinion(beer.id)
        },

        Evaluation: {
            beer: (eval, filters, ctx) => beerService.getBeer(eval.beerId)
        }
    }
});