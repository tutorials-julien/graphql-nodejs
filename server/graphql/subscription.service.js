'use strict';

const { PubSub } = require('apollo-server');

const pubsub = new PubSub();

module.exports = {
    pubsub,
    DRINK_ADDED: 'DRINK_ADDED',
    CLIENT_ADDED: 'CLIENT_ADDED'
};