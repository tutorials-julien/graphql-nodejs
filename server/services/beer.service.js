const { Beer } = require('../database');
const { Op } = require('sequelize');

async function getBeers({ id, minAlcohol, maxAlcohol, minPrice, maxPrice, name } = {}) {
    const where = {};

    id && (where.id = {
        [Op.eq]: id
    });

    if (minAlcohol && maxAlcohol) {
        where.alcohol = {
            [Op.between]: [minAlcohol, maxAlcohol]
        }
    } else if (minAlcohol) {
        where.alcohol = {
            [Op.gte]: minAlcohol
        }
    } else if (maxAlcohol) {
        where.alcohol = {
            [Op.lte]: maxAlcohol
        }
    }

    if (minPrice && maxPrice) {
        where.price = {
            [Op.between]: [minPrice, maxPrice]
        }
    } else if (minPrice) {
        where.price = {
            [Op.gte]: minPrice
        }
    } else if (maxPrice) {
        where.price = {
            [Op.lte]: maxPrice
        }
    }

    name && (where.name = {
        [Op.substring]: name
    });

    return await Beer.findAll({
        where
    });
}

async function getBeer(id) {
    return await Beer.findByPk(id);
}

async function createBeer(beer) {
    return await Beer.create(beer);
}

async function updateBeer(id, beer) {
    const beerInstance = await Beer.findByPk(id);

    if (!beerInstance) {
        throw new Error('BEER_NOT_FOUND');
    }

    return await beerInstance.update(beer, {
        returning: true
    });
}

async function deleteBeer(id) {
    return await Beer.destroy({
        where: { id }
    });
}


module.exports = {
    getBeer,
    getBeers,
    updateBeer,
    createBeer,
    deleteBeer
};