const { Client } = require('../database');
const { Op } = require('sequelize');

const { pubsub, CLIENT_ADDED } = require('../graphql/subscription.service');

async function getClients({ id, firstname, lastname, email } = {}) {
    const where = {};

    id && (where.id = {
        [Op.eq]: id
    });

    firstname && (where.firstname = {
        [Op.substring]: firstname
    });

    lastname && (where.lastname = {
        [Op.substring]: lastname
    });

    email && (where.email = {
        [Op.substring]: email
    });

    return await Client.findAll({
        where
    });
}

async function getClient(id) {
    return await Client.findByPk(id);
}

async function createClient(client) {
    const added = await Client.create(client);
    
    pubsub.publish(CLIENT_ADDED, { clientAdded: added.toJSON() });

    return added;
}

async function updateClient(id, client) {
    const clientInstance = await Client.findByPk(id);

    if (!clientInstance) {
        throw new Error('CLIENT_NOT_FOUND');
    }

    return await clientInstance.update(client, {
        returning: true
    });
}

async function deleteClient(id) {
    return await Client.destroy({
        where: { id }
    });
}

module.exports = {
    getClient,
    getClients,
    createClient,
    updateClient,
    deleteClient
};