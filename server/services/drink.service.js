const { Drink, Beer, Client } = require('../database');
const { Op } = require('sequelize');

const { pubsub, DRINK_ADDED } = require('../graphql/subscription.service');

async function getDrinks({ id, isPayed, beerId, clientId } = {}) {
    const where = {};

    id && (where.id = {
        [Op.eq]: id
    });

    clientId && (where.clientId = {
        [Op.eq]: clientId
    });

    beerId && (where.beerId = {
        [Op.eq]: beerId
    });

    isPayed && (where.isPayed = {
        [Op.eq]: isPayed
    });

    return await Drink.findAll({
        where
    })
}
async function getDrink(id) {
    return await Drink.findByPk(id);
}

async function createDrink(drink) {
    const added = await Drink.create(drink);
    pubsub.publish(DRINK_ADDED, { drinkAdded: added.toJSON() });
    return added;
}

async function updateDrink(id, drink) {
    const drinkInstance = await Drink.findByPk(id);

    if (!drinkInstance) {
        throw new Error('DRINK_NOT_FOUND');
    }

    return await drinkInstance.update(drink, {
        returning: true
    });
}

async function deleteDrink(id) {
    return await Drink.destroy({
        where: { id }
    });
}

async function getFacture(clientId) {
    const allDrinks = await Drink.findAll({
        where: {
            clientId: {
                [Op.eq]: clientId
            }
        },
        include: [{
            model: Beer
        }]
    });

    const totalPrice = allDrinks
        .filter(drink => !drink.isPayed)
        .reduce((total, drink) => total + (drink.quantity * drink.beer.price), 0);

    return totalPrice;
}

async function payAll(clientId) {
    if (!clientId) {
        throw 'Please provide clientId.'
    }

    await Drink.update(
        {
            isPayed: true
        }, {
            where: {
                clientId: {
                    [Op.eq]: clientId
                }
            }
        }
    );

    return Client.findByPk(clientId);
}

module.exports = {
    getDrink,
    getDrinks,
    createDrink,
    updateDrink,
    deleteDrink,
    getFacture,
    payAll
};