const beerService = require('./beer.service');
const drinkService = require('./drink.service');
const clientService = require('./client.service');
const opinionService = require('./opinion.service');

module.exports = {
    beerService,
    drinkService,
    clientService,
    opinionService
};