const { Opinion } = require('../database');
const { Op } = require('sequelize');

async function getOpinions({ id, minNote, maxNote, beerId, clientId } = {}) {
    const where = {};

    id && (where.id = {
        [Op.eq]: id
    });

    beerId && (where.beerId = {
        [Op.eq]: beerId
    });

    clientId && (where.clientId = {
        [Op.eq]: clientId
    });

    if (minNote && maxNote) {
        where.note = {
            [Op.between]: [minNote, maxNote]
        }
    } else if (minNote) {
        where.note = {
            [Op.gte]: minNote
        }
    } else if (maxNote) {
        where.note = {
            [Op.lte]: maxNote
        }
    }

    return await Opinion.findAll({
        where
    })
}

async function getOpinion(id) {
    return await Opinion.findByPk(id);
}

async function createOpinion(opinion) {
    const where = {
        beerId: opinion.beerId,
        clientId: opinion.clientId
    };

    const opinions = await Opinion.findAll({
        where
    });

    if (opinions.length > 0) {
        throw new Error('OPINION_ALREADY_CREATED');
    }

    return await Opinion.create(opinion);
}

async function updateOpinion(id, opinion) {
    const opinionInstance = await Opinion.findByPk(id);

    if (!opinionInstance) {
        throw new Error('OPINION_NOT_FOUND');
    }

    const { beerId, clientId } = opinionInstance;

    return await opinionInstance.update(Object.assign(opinion, { clientId, beerId }), {
        returning: true
    });
}

async function deleteOpinion(id) {
    return await Opinion.destroy({
        where: { id }
    });
}

async function getAverageOpinion(beerId) {
    const opinions = await Opinion.findAndCountAll({
        where: {
            beerId
        }
    });

    return {
        averageNote: opinions.rows.reduce((total, row) => total + row.note, 0) / opinions.count,
        count: opinions.count,
        opinions: opinions.rows,
        beerId
    };
}

module.exports = {
    getOpinion,
    getOpinions,
    createOpinion,
    updateOpinion,
    deleteOpinion,
    getAverageOpinion
};